# Laravel Comments

## Installation

```
$ git clone https://craftik@bitbucket.org/craftik/laravel-comments.git
$ cd laravel-comments
$ sh first_start.sh
```

## Starting

```
$ docker-compose up -d
```

## Migrate

```
$ docker-compose run --rm php php artisan migrate --seed
```

## Running tests

```
$ docker-compose run --rm php ./vendor/bin/phpunit
```
