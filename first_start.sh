#!/bin/bash

cp src/.env.local src/.env

docker-compose run --rm  php composer install
docker-compose run --rm  php php artisan key:generate
