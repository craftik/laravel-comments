<?php

namespace Tests\Feature;

use App\Models\Comment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    public function testIndexComment()
    {
        factory(Comment::class)->create([
            'body' => 'First Comment'
        ]);

        factory(Comment::class)->create([
            'body' => 'Second Comment'
        ]);

        $response = $this->get(route('comments.index'))
            ->assertStatus(200)
            ->assertJson([
                ['body' => 'Second Comment'],
                ['body' => 'First Comment']
            ])
            ->assertJsonStructure([
                '*' => ['id', 'body', 'replies', 'time'],
            ]);
    }

    public function testStoreComment()
    {
        $data = [
            'body' => 'Test Comment'
        ];

        $response = $this->post(route('comments.store'), $data)
            ->assertStatus(201)
            ->assertJson([
                'body' => 'Test Comment'
            ])
            ->assertJsonStructure([
                'id', 'body', 'replies', 'time'
            ]);
    }

    public function testUpdateComment()
    {
        $comment = factory(Comment::class)->create();

        $data = [
            'body' => 'Test Update'
        ];

        $response = $this->put(route('comments.update', $comment->id), $data)
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Test Update'
            ])
            ->assertJsonStructure([
                'id', 'body', 'replies', 'time'
            ]);
    }

    public function testDestroyComment()
    {
        $comment = factory(Comment::class)->create();

        $response = $this->delete(route('comments.destroy', $comment->id))
            ->assertStatus(204);
    }
}
